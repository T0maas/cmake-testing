# Testing project in cmake with COMPONENTS
To use that project install it first
```
git clone https://gitlab.com/T0maas/cmake-testing
cd cmake-testing
mkdir build
cd build
cmake ..
make
sudo make install
```

## Testing
For use in your testing project you can use following cmake code:
```
cmake_minimum_required(VERSION 3.21)
project(test)
find_package(ltk COMPONENTS Core Networking REQUIRED)
add_executable(main main.cc)
target_link_libraries(main ltk::Core ltk::Networking)
target_include_directories(main PUBLIC ltk)
```

Testing project will be linked with libCore and libNetworking shared libraries and include directory will be set (default `-I/usr/include/ltk`).
